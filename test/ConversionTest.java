import org.junit.Assert;
import org.junit.Test;

public class ConversionTest {
	@Test
	public void convertAbcTest() {
		StringConverter converter = new StringConverter();
		int result = converter.convertLettersToNumbers("abc");
		
		Assert.assertEquals(123, result);
	}
	@Test
	public void checkLowerCase() {
		StringConverter converter = new StringConverter();
		int result = converter.convertLettersToNumbers("ABC");
		
		Assert.assertEquals(246, result);
	}
	@Test
	public void testInvalidInput() {
		StringConverter converter = new StringConverter();
		int result = converter.convertLettersToNumbers("?");
		
		Assert.assertEquals(-1, result);
	}
	@Test
	public void testcz() {
		StringConverter converter = new StringConverter();
		int result = converter.convertLettersToNumbers("cz");
		
		Assert.assertEquals(326, result);
	}
	@Test
	public void checkaBcT() {
		StringConverter converter = new StringConverter();
		int result = converter.convertLettersToNumbers("aBcT");
		
		Assert.assertEquals(14340, result);
	}
}
