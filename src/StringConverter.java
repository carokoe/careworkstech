public class StringConverter {

	public int convertLettersToNumbers(String input) {
		String result = new String();
		for (int i = 0; i < input.length(); i++) {
			char currentLetter = input.charAt(i);
			if (currentLetter >= 'a' && currentLetter <= 'z') {
				System.out.print(currentLetter);
				int currentValue = currentLetter - 'a' + 1;
				System.out.print(" --> ");
				System.out.println(currentValue);
				result += String.valueOf(currentValue);
			} else if (currentLetter >= 'A' && currentLetter <= 'Z') {
				System.out.print(currentLetter);
				int currentValue = (currentLetter - 'A' + 1) * 2;
				System.out.print(" --> ");
				System.out.println(currentValue);
				result += String.valueOf(currentValue);
			} else {
				return -1;
			}
		}
		return Integer.parseInt(result);
	}
}

