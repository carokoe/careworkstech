# README #
Java Unit Testing Exercise
Write a StringConverter class in Java with a method named convertLettersToNumbers. 
Also write Unit tests that cover the logic in the method.
It takes a String as input and converts it to an integer.
Signature:
int convertLettersToNumbers (String input)
It replaces each letter with its place in the alphabet (a is 1, z is 26).
Capital letters are doubled (A is 2, Z is 52).
For unexpected or invalid input (non-alpha characters, etc.), the method should return
-1.
Examples:
"abc" --> 123
"cz" --> 326
"aBcT" --> 14340
For this assignment, provide both a Java class with this method implemented as well as
a JUnit class that tests this method. We want to see how you think about testing. The
tests need to prove that this method is implemented as expected and therefore need to
describe and cover the behavior completely. Be sure to consider edge cases and
potential sources of bugs.
This README would normally document whatever steps are necessary to get your application up and running.

///////////////////

Assignment executed using Eclipse 4.6 for Mac OS X 

StringConverter.java contains the Java class StringConverter with the method convertLettersToNumbers.

ConversionTest.java contains the Java class ConversionTest with JUnit tests checking that all required specs are met.
(All tests pass.)